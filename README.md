# DFRobot_PH_ESP32 #

This is a library for DFRobot PH sensor for arduino ESP32.
The sensor have a library from DFRobot but this library is not working out of the box for ESP32.
GreenPonik make a library that is working with ESP32 (https://github.com/GreenPonik/DFRobot_ESP_PH_BY_GREENPONIK) and is an exact replica of the DFRobot library.
But...
What I do not like from the original library :

- The calibration process is done by serial input (terminal connected to the chip). Personnaly I have a TFT screen attached to my project and I want to use the TFT screen for calibration. 
- The EEPROM save and restore seems not to work, even in the GreenPonik library.
- The analog reading must be done outside and feed to the library object. This could be done directly inside functions.
- They ask for temperature reading for temperature correction of the reading but do nothing with the value.

What this library improved :

- Saving values in SPIFFs.
- Add methods to calibrate the probe
- Do the reading directly in the library class
- Remove all reference to temperature to avoid the "impression" or the "confusion" about using the temperature for ph value correction.

What this library does NOT improved:

- All libraries take a temperature variable to do correction, but none implement the temperature correction (they do nothing with the temperature variable). **This library is no exception.** If somebody have this code somewhere, I'll be glad to include the code in mine.

## DISCLAIMER
This library is based and inspired by both library mentionned above, the original and GreenPonik:

- https://github.com/DFRobot/DFRobot_PH
- https://github.com/GreenPonik/DFRobot_ESP_PH_BY_GREENPONIK

## Tested on ##
This library project had been tested against an ESP32 (nodemcu-32s to be exact). The probe (ph probe v2 PRO) is connected on 5V and pin 39.

# Usage #
...TBD...